## Ciclo normal del uso de git flow

cree el repositorio

Gitflow 

### inicializar gitflow 
main  - Va a contener lo que va a pasar a produccion
hotfix - Va a ser utilizado para cambios pequeños
release - Es la rama que contiene todo antes de una entrega
develop - Es la rama donde se van a cumulando las feature (características)
feature - Es una característica

### Al iniciar contamos con  
master  - Va a contener lo que va a pasar a produccion
develop - Es la rama donde se van a cumulando las feature (características)

### Cuando quiero agregar una caracteristica nueva (cualquier funcionalidad)
tengo que agregar una nueva feature

### Cuando termino la nueva funcionalidad y la quiero integrar a develop para que este disponible 
para todos 

finalizo el feature 
Nota: esto me devuelve a develop, para que pueda hacer una nueva feature a partir de el estado
actual de develop

### Hotfix
si tengo algun pequeño error ya sea en master lo puedo usar 

### Bugfix 
Lo mismo que hotfix pero en desarrollo

### Release 
se utiliza cuando vamos a hacer un pase a producción 

### Master 

Contiene las versiones de producción de nuestro proyecto

## ACLARACIONES 

PUEDES utilizar hotfix en master, bugfix en develop y en una feature...

Siempre que el hotfix termino en la misma rama que empezo.
